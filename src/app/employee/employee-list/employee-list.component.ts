import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import empData from 'src/app/empData.json';

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.css']
})
export class EmployeeListComponent implements OnInit {

  isOpened: boolean = false;
  successMsg: boolean = false;
  empList: any = []
  itemsAfterDeleted: any = [];

  constructor() { }

  // add employee function start
  addEmployee() {
    this.empList = [...empData];
    this.itemsAfterDeleted = [...empData];
  }
  // add employee function end

  // view employee func start
  viewEmployee(emp: any) {
    this.isOpened = true;
    this.updateEmpDetailForm.controls['id'].setValue(emp.id);
    this.updateEmpDetailForm.controls['firstname'].setValue(emp.firstname);
    this.updateEmpDetailForm.controls['lastname'].setValue(emp.lastname);
    this.updateEmpDetailForm.controls['age'].setValue(emp.age);
    this.updateEmpDetailForm.controls['jobtitle'].setValue(emp.jobtitle);
    this.updateEmpDetailForm.controls['startdate'].setValue(emp.startdate);
    this.updateEmpDetailForm.controls['enddate'].setValue(emp.enddate);
  }
  // end of view employee func

  // delete employee
  deleteEmployee(arr: any, id: any) {
    const item = arr.findIndex((emp: any) => emp.id === id);
    if(this.itemsAfterDeleted.length>1)
    {
    this.itemsAfterDeleted = this.itemsAfterDeleted.filter((item: any) => item.id != id);
    }
    if (arr.length <= 1) {
      alert('You cannot delete the last record!');
    } else if (item > -1) {
      arr.splice(item, 1);
    }
    return arr;
  }
  // end of delete employee

  // update detail modal
  updateEmpDetailForm = new FormGroup({
    id: new FormControl('', [Validators.required]),
    firstname: new FormControl('', [Validators.required]),
    lastname: new FormControl('', [Validators.required]),
    age: new FormControl('', [Validators.required]),
    jobtitle: new FormControl('', [Validators.required]),
    startdate: new FormControl('', [Validators.required]),
    enddate: new FormControl('')
  });

  get updateFrmValition() {
    return this.updateEmpDetailForm.controls;
  }

  updateEmp() {
    let frmValues = this.updateEmpDetailForm.value;
    let foundIndex = this.empList.findIndex((e: any) => e.id == frmValues.id);
    this.empList[foundIndex] = frmValues;
    this.itemsAfterDeleted[foundIndex] = frmValues;
    this.isOpened = false;
    this.successMsg = true;
    setTimeout(() => {
      this.successMsg = false;
    }, 1500)
  }
  // end of update detail modal

  // start of close modal
  closeModal() {
    this.updateEmpDetailForm.reset({});
    this.isOpened = false;
  }
  // end of close modal


  //  filter function start here
  filterForm = new FormGroup({
    name: new FormControl(''),
    age: new FormControl(''),
    jobtitle: new FormControl(''),
    startdate: new FormControl(''),
    enddate: new FormControl('')
  });

  filterEmp() {
    if (!this.filterForm.controls['name'].value && !this.filterForm.controls['age'].value && !this.filterForm.controls['jobtitle'].value && !this.filterForm.controls['startdate'].value && !this.filterForm.controls['enddate'].value) {
      alert('Please select atleast one filter.');
    } else {
      this.empList = [...this.itemsAfterDeleted];
      console.log('i', this.itemsAfterDeleted, this.empList, this.filterForm.controls)
      if (this.filterForm.controls['name'].value != '' && this.filterForm.controls['name'].value != null) {
        this.empList = this.empList.filter((item: any) => { return item.firstname.toLowerCase() == this.filterForm.controls['name'].value?.toLocaleLowerCase() || item.lastname.toLowerCase() == this.filterForm.controls['name'].value?.toLocaleLowerCase() });
      }
      if (this.filterForm.controls['age'].value != '' && this.filterForm.controls['age'].value != null) {
        this.empList = this.empList.filter((item: any) => { return item.age == this.filterForm.controls['age'].value });
      }
      if (this.filterForm.controls['jobtitle'].value != '' && this.filterForm.controls['jobtitle'].value != null) {
        this.empList = this.empList.filter((item: any) => { return item.jobtitle == this.filterForm.controls['jobtitle'].value });
      }
      if (this.filterForm.controls['startdate'].value != '' && this.filterForm.controls['startdate'].value != null && this.filterForm.controls['enddate'].value != '' && this.filterForm.controls['enddate'].value != null) {
        console.log(this.empList[0].enddate, this.filterForm.controls['enddate'].value)
        this.empList = this.empList.filter((item: any) => { return item.startdate == this.filterForm.controls['startdate'].value && item.enddate == this.filterForm.controls['enddate'].value });
      } else {
        if (this.filterForm.controls['startdate'].value != '' && this.filterForm.controls['startdate'].value != null) {
          this.empList = this.empList.filter((item: any) => { return item.startdate == this.filterForm.controls['startdate'].value });
        }
        if (this.filterForm.controls['enddate'].value != '' && this.filterForm.controls['enddate'].value != null) {
          this.empList = this.empList.filter((item: any) => { return item.enddate == this.filterForm.controls['enddate'].value });
        }
      }
    }
  }

  resetFilter() {
    this.filterForm.reset({});
    this.filterForm.controls['jobtitle'].setValue('');
    this.empList = [...this.itemsAfterDeleted];
  }
  // end of filter function

  ngOnInit(): void {
  }

}
